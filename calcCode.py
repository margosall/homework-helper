SUB = str.maketrans("0123456789", "₀₁₂₃₄₅₆₇₈₉")
multiplierValues = [0x5,0x7,0xB,0xD]
determined = []
notDetermined = []

def calcFunc(code,multiplier):
	out = code 
	while(out < 0x10000000):
		out *= multiplier
	# print(hex(out))
	for i in range(0,8):
		tempValue = (out & (0xF << i*4)) >> i*4
		# print(tempValue)
		if not tempValue in determined:
			determined.append(tempValue) 

	out= int(out/3)
	for i in range(0,8):
		tempValue = (out & (0xF << i*4)) >> i*4
		# print(tempValue)
		if(not ((i==7) and (tempValue == 0))): 
			if (not tempValue in determined) and (not tempValue in notDetermined):
				notDetermined.append(tempValue) 

	notDetermined.sort()
	determined.sort()
	# print(determined)
	# print(notDetermined)

	# print()

studentCode = 1234
# studentCode = int(input("Enter your student code: "))
functions = []
def printNibble(byte):
	for i in range(3,-1,-1):
		print((byte & (1 << i)) >> i,end='')

for i in range(0,4):
	determined = []
	notDetermined = []
	calcFunc(studentCode,multiplierValues[i])
	print(("f" + str(i+1) + "(x1,x2,x3,x4)").translate(SUB),end='')
	print("=Σ(",end='')
	#print(determined)
	for j in range(len(determined)):
		print(determined[j],end='')
		print("," if not(j==len(determined)-1) else ")₁(",end='')

	for j in range(len(notDetermined)):
		print(notDetermined[j],end='')
		print("," if not(j==len(notDetermined)-1) else ")_\r\n",end='')
	functions.append([determined,notDetermined])

functionSelection = []
for num in range(2**4):
	string = ""
	# printNibble(num)
	# print(" ",end='')
	for func in range(4):
		if num in functions[func][0]: #Determined
			string+='1'
			# print("1",end='')
		elif num in functions[func][1]: #Notdetermined
			string+='-'
			# print("-",end='')
		else:							#0
			string+="0"
			# print("0",end='')
	# print()
	#printNibble(num)
	functionSelection.append(string)
	#print(" ",functionSelection[num])

for num in range(2**4):
	printNibble(num)
	print(" ",functionSelection[num])
#print(functions)